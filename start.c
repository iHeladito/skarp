#include <stdio.h>
#include <stdlib.h>

extern void filem();

int main(){
  printf("Skarp is a text editor for using from command line interface\n");
  // Example
  // Lua 5.1.5  Copyright (C) 1994-2012 Lua.org, PUC-Rio
  // >
  // Skarp 2021-2021 github.com/iHeladito/skarp
  // >

  filem("file.txt");
  filem("file2.txt");

  return 0;
}
