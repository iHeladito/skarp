// FILEManipulate.C

#include <stdio.h>
#include <stdlib.h>
#define NAMEF char namef[] // Name of the file
#define FN1 FILE *fn1; fn1 = fopen(namef,"r"); 

int foundf(); // return 1(true: the file existing) or 0(false: the file not existing)

void filem(NAMEF){
  FN1

  // Started to read the file
  if(foundf(namef) == 1){    
    printf("Read the file '%s'\n",namef);
    char tmpread; // temporal read file
    while((tmpread = fgetc(fn1)) != EOF){ putchar(tmpread); }

  } // And no, create
  else{
    printf("Create the file '%s'\n",namef);

  }

  return;
}

int foundf(NAMEF){ // found file
  FN1
  if(fn1 != NULL){
    fclose(fn1);
    return 1;

  }else{
    fn1 = fopen(namef,"w");
    fclose(fn1);
    return 0;

  }
}
