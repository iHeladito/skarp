CC := gcc
CCF := --all-warnings -Wall -std=c99

all: install clean

install:
	$(CC) $(CCF) *.c -c
	$(CC) $(CCF) *.o -o skarp

clean:
	rm *.o
